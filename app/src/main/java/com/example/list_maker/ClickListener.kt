package com.example.list_maker

import android.view.View

interface ClickListener {
    fun onClick(vista: View, index: Int)
}
package com.example.list_maker

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
     var lista: RecyclerView?=null
    var adaptador:AdaptadorCustom?=null
    var layoutManager: RecyclerView.LayoutManager?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val platillos = ArrayList<Platillo>()

        platillos.add(Platillo(nombre = "Platillo1", precio = 24.60, rating = 4.8f, foto = R.drawable.img01))
        platillos.add(Platillo(nombre = "Platillo2", precio = 34.60, rating = 1.8f, foto=R.drawable.img02))
        platillos.add(Platillo(nombre = "Platillo3", precio = 44.60, rating = 2.8f, foto=R.drawable.img03))
        platillos.add(Platillo(nombre = "Platillo4", precio = 54.60, rating = 3.8f, foto=R.drawable.img04))
        platillos.add(Platillo(nombre = "Platillo5", precio = 64.60, rating = 5.8f, foto=R.drawable.img05))
        platillos.add(Platillo(nombre = "Platillo5", precio = 64.60, rating = 5.8f, foto=R.drawable.img06))

        lista=findViewById(R.id.idLista)
        lista?.setHasFixedSize(true)

        layoutManager=LinearLayoutManager(this)
        lista?.layoutManager=layoutManager

        adaptador=AdaptadorCustom(platillos,object : ClickListener{
            override fun onClick(vista: View, index: Int) {
                Toast.makeText(applicationContext,platillos.get(index).nombre, Toast.LENGTH_SHORT).show()
            }

        })
        lista?.adapter=adaptador

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}

package com.example.list_maker

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class AdaptadorCustom(items: ArrayList<Platillo>,var listener: ClickListener):RecyclerView.Adapter<AdaptadorCustom.ViewHolder>(){

    var items: ArrayList<Platillo>?=null
    init {
        this.items=items
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdaptadorCustom.ViewHolder {
        val vista= LayoutInflater.from(parent.context).inflate(R.layout.template_patillo, parent, false)
        val viewHolder= ViewHolder(vista,listener)

        return  viewHolder
    }

    override fun getItemCount(): Int {
        return  items?.count()!!

    }

    override fun onBindViewHolder(holder: AdaptadorCustom.ViewHolder, position: Int) {
        val item =items?.get(position)
        holder.foto?.setImageResource(item?.foto!!)
        holder.nombre?.text=item?.nombre
        holder.precio?.text="$"+item?.precio.toString()
        holder.rating?.rating=item?.rating!!

    }

    class ViewHolder(vista: View,listener: ClickListener):RecyclerView.ViewHolder(vista), View.OnClickListener{


        var vista= vista
        var foto:ImageView?=null
        var nombre:TextView?=null
        var precio:TextView?=null
        var rating:RatingBar?=null
        var listener:ClickListener?=null

        init {
            foto= vista.findViewById(R.id.idFoto)
            nombre= vista.findViewById(R.id.idNombre)
            precio= vista.findViewById(R.id.idPrecioP)
            rating= vista.findViewById(R.id.idRating)
            this.listener=listener
            vista.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            this.listener?.onClick(v!!,adapterPosition)
        }

    }

}